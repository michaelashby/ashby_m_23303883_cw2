<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questions')->insert([
                ['id' => 1, 'question' => "Name two tyoes of questionnaires"],
                ['id' => 2, 'question' => "What is an open question?"],
                ['id' => 3, 'question' => "What is a closed question?"],
        ]);
    }
}
