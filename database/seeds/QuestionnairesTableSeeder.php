<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questionnaires')->insert([
          ['id' => 1, 'questionnairetitle' => "Questionnaire 1"],
          ['id' => 2, 'questionnairetitle' => "Questionnaire 2"],
          ['id' => 3, 'questionnairetitle' => "Questionnaire 3"],
        ]);
    }
}
