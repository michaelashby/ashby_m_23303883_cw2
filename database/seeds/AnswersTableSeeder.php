<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
          ['id' => 1, 'question' => 1, 'answer' => 'Computer'],
          ['id' => 2, 'question' => 1, 'answer' => 'Phone'],
          ['id' => 3, 'question' => 1, 'answer' => 'Multiple Choice'],
          ['id' => 4, 'question' => 1, 'answer' => 'Survey'],
          ['id' => 5, 'question' => 2, 'answer' => 'An open question allows the user to an explain an answer in detail'],
          ['id' => 6, 'question' => 2, 'answer' => 'Answers are explained in detail'],
          ['id' => 7, 'question' => 2, 'answer' => 'Answers just yes or no'],
          ['id' => 8, 'question' => 3 , 'answer' => 'Allows the user to give a detailed explanation'],
          ['id' => 9, 'question' => 3, 'answer' => 'Just answers yes or no'],

        ]);
    }
}
