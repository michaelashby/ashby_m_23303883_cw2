@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h1>You are logged in!</h1>

                  <h2>
                      <a href="http://localhost:8000/questionnaires">Questionnaire</a>
                  <h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
