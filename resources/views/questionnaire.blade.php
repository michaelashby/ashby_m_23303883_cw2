@extends('layouts.master')

@section('title', 'Questionnaire')

@section('content')
  <h1>Please fill out the questionnaire!</h1>


  <h4>When done press submit</h4>

    {{ Form::open(array('action' => 'ResponseController@store')) }}

      <section>
          @if (isset ($questions))

              <ul>
                @foreach($questions as $question)
                  {!! Form::label($question->id, $question->question) !!}
                  {!! Form::select($question->id, $question['answers']) !!}
                  <br/> <br/>
                @endforeach
              </ul>
          @else

          @endif
      </section>
        <div class="row">
            {!! Form::submit('Submit Questionnaire', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}

@endsection
