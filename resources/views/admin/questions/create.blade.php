<!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Admin - create questions</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <header class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <ul class="nav navbar-nav">
                      <a class="navbar-brand" href="#">Admin</a>
                      <li class="active"><a href="/">Questions</a></li>
                  </ul>
              </div>
          </nav>
      </header>
      <article class="row">
          <h1>Create a new question</h1>

          <!-- form goes here -->
          {!! Form::open(['url' => '/questions']) !!}

          {!! Form::hidden('questionnaire',"") !!}

          <div class="form-group">
        {!! Form::label('question', 'Title:') !!}
        {!! Form::text('question', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Add question', ['class' => 'btn btn-primary form-control']) !!}
    </div>


          {!! Form::close() !!}


      </article>
  </div><!-- close container -->

  </body>
  </html>
