@extends('layouts.master')

@section('title', 'Questions')

@section('content')
@foreach($questions as $question)
<p>Question {{$question->id}}: {{$question->question}}</p>
@endforeach


    <section>
        @if (isset ($questionnaires))

            <ul>
              @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questions/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
              @endforeach
            </ul>
        @else
    
        @endif
    </section>


    {{ Form::open(array('action' => 'QuestionsController@create', 'method' => 'get')) }}
        <div class="row">
            {!! Form::submit('Add Question', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}
@endsection
