<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    protected $fillable = [
      'name',
      'age',
      'student number',
    ];
}
