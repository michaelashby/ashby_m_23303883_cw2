<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\questions;
use App\questionnaire;
use App\answers;


class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questions = questions::all();


      foreach($questions as $question) {
        $answers = answers::where('question', $question->id)->get();
        $answers_format = array();
        foreach($answers as $answer) {
          $answers_format[$answer->id] = $answer->answer;
        }
        $question['answers'] = $answers_format;
      }
        return view('questionnaire')->with('questions', $questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.questionnaire.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $questionnaire = questionnaire::create($request->all());
        return redirect ("/questionnaires" . $questionnaire->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = questionnaire::findOrFail($id);
        $questions = questions::where('questionnaire', $id)->get();

        foreach($questions as $question) {
          $answers = answers::where('question', $question->id)->get();
          $question['answers'] = $answers;

        }

        return $questions;
        return view('admin.questionnaire.show')->with('questionnaire', $questionnaire)->with('questions', $questions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
