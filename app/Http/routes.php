<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'HomeController');
Route::resource('/questionnaires', 'QuestionnaireController');

Route::auth();

Route::get('/home', 'HomeController@index');
Route::resource('/admin/questions', 'QuestionsController');
Route::resource('/questions',  'QuestionsController');
Route::resource('/responses',  'ResponseController');
