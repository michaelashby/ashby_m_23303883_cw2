<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
  protected $fillable = [
    'question',
    'questionnairetitle',
  ];
}
